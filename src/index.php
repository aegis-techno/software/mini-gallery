<?php
ini_set('display_errors', true);

date_default_timezone_set("Europe/Paris");

require_once("scripts/utils.php");
require_once("scripts/session_management.php");
require_once("scripts/directory_tools.php");
require_once("templates/templates.php");
require_once("scripts/main.php");

error_reporting(E_ALL);
initSession();

if (!isset($_GET['folder'])) {
    $_GET['folder'] = "";
}

print_page();

?>
