function hideDiv() {
    if (document.getElementById) {
        document.getElementById('hideshow').style.visibility = 'hidden';
        document.getElementById('viewer').src = 'public/images/white.jpg';
    } else {
        if (document.layers) {
            document.hideshow.visibility = 'hidden';
            document.viewer.src = 'public/images/white.jpg';
        } else {
            document.all.hideshow.style.visibility = 'hidden';
            document.all.viewer.src = 'public/images/white.jpg';
        }
    }

}

function showDiv(filename) {
    if (document.getElementById) {
        document.getElementById('hideshow').style.visibility = 'visible';
        document.getElementById('viewer').src = filename;
        document.getElementById('viewer').filename = filename;
        document.getElementById('viewer').parentNode.href = filename;
        var pos = filename.lastIndexOf('/');
        document.getElementById('picture_name').innerHTML = filename.substr(pos + 1, filename.substr(pos + 1).length - 4);
    } else {
        if (document.layers) {
            document.hideshow.visibility = 'visible';
            document.viewer.src = filename;
            document.viewer.filename = filename;
            document.viewer.parentNode.href = filename;
            document.picture_name.innerHTML = filename;
        } else {
            document.all.hideshow.style.visibility = 'visible';
            document.all.viewer.src = filename;
            document.all.viewer.filename = filename;
            document.all.viewer.parentNode.href = filename;
            document.all.picture_name.innerHTML = filename;
        }
    }
}

function next() {
    var filename = document.getElementById('viewer').filename;
    var divs = document.getElementsByName('vig');

    for (var i = 0; i < divs.length; i++) {
        if (divs[i].href === "javascript:showDiv('" + filename + "')") {
            var next;
            if (i + 1 < divs.length)
                next = divs[i + 1];
            else
                next = divs[0];
            hideDiv();
            window.location.href = next.href
        }
    }
}

function previous() {
    var filename = document.getElementById('viewer').filename;
    var divs = document.getElementsByName('vig');

    for (var i = 0; i < divs.length; i++) {
        if (divs[i].href === "javascript:showDiv('" + filename + "')") {
            var previous;
            if (i - 1 < 0)
                previous = divs[divs.length - 1];
            else
                previous = divs[i - 1];
            hideDiv();
            window.location.href = previous.href
        }
    }
}

$(document).ready(function () {

    var divs = $("#subFolder ul li");


    var alphabeticallyOrderedDivs = divs.sort(function (a, b) {
        return $(a).find("a").text() > $(b).find("a").text();
    });
    $("#subFolder ul").html(alphabeticallyOrderedDivs);


});