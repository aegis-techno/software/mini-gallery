<?php

function print_header()
{
    $title = get_config()['login_message'];
    $sid = session_id();
    require dirname(__FILE__) . '/layout/header.php';
}

function print_logon()
{
    require dirname(__FILE__) . '/session/logon.php';
}

function print_hello()
{
    $pseudo = $_SESSION['pseudo'];
    $sub_title = get_config()['main_subtitle'];
    require dirname(__FILE__) . '/session/hello.php';
}

function print_goodbye()
{
    require dirname(__FILE__) . '/session/goodbye.php';
}

function print_footer()
{
    require dirname(__FILE__) . '/layout/footer.php';
}

function print_subfolders($current_folder_path, $sub_folders, $prev)
{
    $sid = session_id();
    require dirname(__FILE__) . '/gallery/sub_folders.php';
}

function print_folder_pictures($repNamePicture, $tab)
{
    $vig_dir = get_config()['vig_dir'];
    require dirname(__FILE__) . '/gallery/folder_pictures.php';
}

?>
