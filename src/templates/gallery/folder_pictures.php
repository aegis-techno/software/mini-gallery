<div id="content">
    <?php
    $compteur = 0;
    $lastdate = "";
    foreach ($tab as $i => $v) {
        $compteur++;
        $ori_picture = "$repNamePicture/$v";
        $vig_picture = "$vig_dir/$repNamePicture/$v";
        if (substr($i, 0, 7) != substr($lastdate, 0, 7))
            echo "<h1> " . get_month(substr($i, 5, 2)) . " " . substr($i, 0, 4) . "</h1><br/>\n\r";
        $lastdate = $i;

        $ext = strtolower(pathinfo($ori_picture, PATHINFO_EXTENSION));

        if ($ext == "png" ||
            $ext == "jpg" ||
            $ext == "jpeg" ||
            $ext == "bmp" ||
            $ext == "tif" ||
            $ext == "tiff" ||
            $ext == "svg" ||
            $ext == "ico" ||
            $ext == "gif" ||
            $ext == "pcd" ||
            $ext == "fpx" ||
            $ext == "jif" ||
            $ext == "jfif" ||
            $ext == "jp2" ||
            $ext == "jpx" ||
            $ext == "j2k" ||
            $ext == "j2c") {
            ?>
            <a name="vig" href="javascript:showDiv('<?php echo $ori_picture ?>')">
                <img src='<?php echo $vig_picture ?>' alt='image'>
            </a>
            <?php
        } else {
            if ($ext == "avi" ||
                $ext == "mov" ||
                $ext == "mpg" ||
                $ext == "mpa" ||
                $ext == "asf" ||
                $ext == "wma" ||
                $ext == "mp2" ||
                $ext == "m2p" ||
                $ext == "mp3" ||
                $ext == "mp4" ||
                $ext == "dif" ||
                $ext == "vob" ||
                $ext == "ogg") {
                ?>
                <a name="vig" href="<?php echo $ori_picture ?>">
                    <img width="80px" height="100px" src="http://365psd.com/images/previews/7d1/psd-movie-clapboard-icon-53295.jpg" alt='image'>
                </a>
                <?php
            }
        }
    }
    ?>

</div>
