<?php

function get_path_from_folder($racine, $path)
{
    if ($path == "") return $racine;

    $subfolders = preg_split("/[\s,]+/", $path);

    $result = $racine;
    foreach ($subfolders as $folder) {
        print_debug("current subfolder to search is [$folder]");

        $subFolderList = get_directories($result);
        $i = 0;
        foreach ($subFolderList as $folderIndex => $folderName) {
            $current = "$result/$folderName";
            if ($i == $folderIndex) {
                $result = $current;
                break;
            }
            $i++;
        }
    }

    return $result;
}

function get_directories($directory)
{
    $vig_dir = get_config()['vig_dir'];

    $tab = array();

    $rep = opendir($directory);
    if (!$rep) return 0;
    while (false !== ($file = readdir($rep))) {
        $current = "$directory/$file";
        if (is_dir($current)) {
            if ($file != $vig_dir && $file != "." && $file != ".." && $file != '') {
                $tab[] = $file;
            }
        }
    }
    closedir($rep);
    return $tab;
}

function get_files($directory)
{
    $vig_dir = get_config()['vig_dir'];

    $tab = array();

    $rep = opendir($directory);
    if (!$rep) return 0;
    while (false !== ($file = readdir($rep))) {
        $current = "$directory/$file";
        if (!is_dir($current)) {
            if ($file != $vig_dir && $file != "." && $file != ".." && $file != '') {
                $tab[] = $file;
            }
        }
    }
    closedir($rep);
    return $tab;
}

function get_sorted_files($directory)
{
    $result = array();

    $files = get_files($directory);
    if ($files == 0) return 0;

    foreach ($files as $i => $file) {
        $current = "$directory/$file";
        $nom[$i] = $file;
        try {
            $exif_date = @exif_read_data($current, 'IFD0', 0);
        } catch (Exception $e) {
            $exif_date = false;
        }

        if ($exif_date != false) {
            if (@array_key_exists('DateTime', $exif_date)) {
                $edate = $exif_date['DateTime'];
                $date[$i] = $edate;
            } else if (@array_key_exists('DateTimeOriginal', $exif_date)) {
                $edate = $exif_date['DateTimeOriginal'];
                $date[$i] = $edate;
            } else {
                // TODO : PRINT EXIF and try other values
//                print_r($exif_date);
//                print_r("<br>");
                print_debug("PRINT EXIF and try other values");

                if (@array_key_exists('FileDateTime', $exif_date)) {
                    $edate = $exif_date['FileDateTime'];
                    $date[$i] = $edate;
                }
            }
        }

        if (!@array_key_exists($i, $nom)) {
            print_debug("ERROR - $i not found in nom.");
        }

        if (!@array_key_exists($i, $date)) {
            $date[$i] = date("Y:m:d H:i:s", filemtime($current));
        }

        $result["$date[$i] $i"] = $nom[$i];

        krsort($result);
    }

    return $result;
}

function get_month($number)
{
    $a_mois = array('00' => 'Error', '0' => 'Error',
        '01' => 'Janvier',
        '02' => 'Fevrier',
        '03' => 'Mars',
        '04' => 'Avril',
        '05' => 'Mai',
        '06' => 'Juin',
        '07' => 'Juillet',
        '08' => 'Aout',
        '09' => 'Septembre',
        '10' => 'Octobre',
        '11' => 'Novembre',
        '12' => 'Decembre');
    return $a_mois[$number];
}

function generate_folder_vig($repNamePicture, $tab, $max_vig)
{
    if (ob_get_level() == 0) ob_start();

    $vig_dir = get_config()['vig_dir'];
    printf("<div id=\"vig-generated\">");
    $vignette_dir = "$vig_dir/" . $repNamePicture;

    if (!is_dir($vignette_dir)) {
        if (!mkdir($vignette_dir, 0770, true)) {
            print("VIG FOLDER ERROR");
        }
    }

    // speed-up initialization
    if (class_exists("Imagick")) {
        $im = new Imagick();
    }

    $max_conv = 0;

    foreach ($tab as $i => $v) {

        $filename = "$repNamePicture/$v";
        filemtime($filename);

        if (!((strtolower(substr($filename, -3)) == "jpg") or (strtolower(substr($filename, -3)) == "jpeg") or (strtolower(substr($filename, -3)) == "png")))
            continue;

        $vigname = "$vignette_dir/$v";
        if (!file_exists($vigname) and ($max_conv < $max_vig)) {
            if (class_exists('Imagick')) {
                try {
                    $im->pingImage($filename);
                    $im->readImage($filename);
                    $im->resizeImage(100, 80, Imagick::FILTER_LANCZOS, 1);
                    $im->writeImage($vigname);
                } catch (ImagickException $e) {
                    print_debug("ImagickException has been catch ...<br>\n");
                }
            } else {
                $src = create_thumb("$repNamePicture/", $v, "$vignette_dir/", "");
                if (!$src) {
                    print_debug("CreateThumb error ...<br>\n");
                }
            }

            if (file_exists($vigname)) {
                $max_conv++;
                printf("Generate a vig .." . $max_conv . " / " . $max_vig . ".<br>\n");
            } else {
                printf("Error during vig generation ...<br>\n");
                break;
            }
        }
        ob_flush();
        flush();
        sleep(0.5);
    }
    printf("</div>\n");

    ob_flush();
    flush();

    ob_end_flush();

}

function create_thumb($full_path, $file, $thumb_path, $thumb_prefix, $width = 96, $height = 72)  // need to check all these functions (which lib is it ?)
{
    ini_set('memory_limit', '512M');
    if (!(function_exists('imagejpeg') and function_exists('imagepng') and function_exists('imagecreatefromjpeg') and function_exists('imagecreatefrompng'))) {
        print_debug("<br>lib gd bad configuration<br>\n");
        if (!function_exists('imagejpeg')) {
            print_debug("<br>imagejpeg() not exist<br>\n");
        }
        if (!function_exists('imagepng')) {
            print_debug("<br>imagepng() not exist<br>\n");
        }
        if (!function_exists('imagecreatefromjpeg')) {
            print_debug("<br>imagecreatefromjpeg() not exist<br>\n");
        }
        if (!function_exists('imagecreatefrompng')) {
            print_debug("<br>imagecreatefrompng() not exist<br>\n");
        }
        return false;
    }
    if (!is_readable($full_path . $file)) {
        print_debug("<br>fichier " . $full_path . $file . " non accessible<br>\n");
        return false;
    }
    switch (strtolower(substr($file, strlen($file) - 3, 3))) {

        case 'jpg':
            $type = 'jpg';
            $source = imagecreatefromjpeg($full_path . $file);
            break;

        case 'png':
            $type = 'png';
            $source = imagecreatefrompng($full_path . $file);
            break;

        default:
            // format inconnu
            print_debug('<br>format inconnu');
            return false;
    }

    $source_width = imagesx($source);
    $source_height = imagesy($source);
    $destination_width = $width;
    $destination_height = $width * $source_height / $source_width;

    // On crée la miniature vide
    if (!$destination = imagecreatetruecolor($width, $height)) {
        print_debug('<br>impossible de creer miniature');
        return false;
    }

    if (!imagecopyresampled($destination, $source, 0, 0, 0, 0, $destination_width, $destination_height, $source_width, $source_height)) {
        print_debug('<br>erreur sur imagecopyresampled');
        return false;
    }
    switch ($type) {
        case 'jpg':
            if (!imagejpeg($destination, $thumb_path . $thumb_prefix . $file)) {
                print_debug('<br>erreur sur imagejpeg<br>');
                return false;
            }
            break;
        case 'png':
            if (!imagepng($destination, $thumb_path . $thumb_prefix . $file)) {
                print_debug('<br>erreur sur imagepng<br>');
                return false;
            }
            break;
        default:
            print_debug('<br>format inconnu<br>');
            return false;
    }
    return true;
}

?>
