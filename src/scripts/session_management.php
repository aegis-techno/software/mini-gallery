<?php

function initSession()
{
    handleSid();

    handleNoUserCase();
    handleDisconnectAction();
    handleLoginAction();
}

/**
 * @return void
 */
function handleLoginAction()
{
    if (isset($_POST['pseudo']) && isset($_POST['mdp'])) {
        $pseudo = htmlspecialchars($_POST['pseudo']);
        $mdp = htmlspecialchars($_POST['mdp']);
        if (isValidUser($pseudo, $mdp)) {
            session_start();
            $_SESSION['mdp'] = $mdp;
            $_SESSION['pseudo'] = $pseudo;
            $_SESSION['folder'] = $_GET['folder'];
            header('Location: index.php?sid=' . session_id());
        }
    }
}

/**
 * @return void
 */
function handleDisconnectAction()
{
    if (isset($_GET['action'])) {
        $action = htmlspecialchars($_GET['action']);

        if ($action == "disconnect") {
            session_destroy();
            header('Location: index.php');
        }
    }
}

/**
 * @return void
 */
function handleSid()
{
    if (isset($_GET['sid'])) {
        session_id($_GET['sid']);
        session_start();
    } else {
        session_start();
    }
}

function handleNoUserCase()
{
    if (count(get_config()['user']) == 0) {
        $_SESSION['mdp'] = "Invite";
        $_SESSION['pseudo'] = "Invite";
    }
}

function isLogin(): bool
{
    return isset($_SESSION['pseudo']);
}

function isValidUser($pseudo, $mdp): bool
{
    return get_config()['user'][$pseudo] == $mdp;
}

?>
