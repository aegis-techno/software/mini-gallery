<?php

function print_page()
{
    print_header();

    if (!isLogin()) {
        print_logon();
    } else {
        print_gallery_page($_GET['folder']);
    }

    print_footer();
}

function print_gallery_page($folder)
{

    print_hello();

    $display_folder_path = get_path_from_folder("./" . get_config()['racine'], $_GET['folder']);

    $dir_content = get_sorted_files($display_folder_path);
    $sub_folders = get_directories($display_folder_path);

    if ($folder != '') {
        $folder = $folder . ',';
        $prev = substr($folder, 0, strrpos($_GET['folder'], ","));
    } else {
        $prev = -1;
    }

    print_subfolders($folder, $sub_folders, $prev);
    generate_folder_vig($display_folder_path, $dir_content, get_config()['max_vig']);
    print_folder_pictures($display_folder_path, $dir_content);

    print_goodbye();

}

?>
