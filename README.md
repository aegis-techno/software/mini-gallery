# MinuteGallery #

## Description ##

This PHP project is a **ready to run** photo gallery **without any database**.
You just need to :

* Copy this code in your website (```git clone git@gitlab.com:aegis-techno/software/mini-gallery.git```),
* Configure the configuration.php file (```vi mini-gallery/src/configuration.ini```)
* Add any photo in the photo's directory (default is ```mini-gallery/src/photos```).

## Requirements ##

This project requires :

* PHP 7.4.0
* exif library
* Imagick library (or imagecreatefromjpeg and imagecreatefrompng functions support)

ou

* Docker

## Configuration ##

The configuration.ini file should look like :

```
; This is a sample configuration file
; Comments start with ';', as in php.ini

[photos_section]
vig_dir = "vig"
racine = "photos"
max_vig = 50

[session_section]
user[Invite] = "Invite"

[template_section]
login_message = "Please, type your password !"
main_title = "My gallery"
main_subtitle = ""
```

## Security status ##

As every personal web project, **this project might not be fully secured**.
Please take your time to have a look and ensure yourself that this project has no possible conflict with your current configuration or your security policy.

## Licence ##

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Third party licence ##

This project also contains a font file (Englebert.ttf) under the SIL Open Font License (OFL).
